import React from 'react';
import { render } from '@testing-library/react';
import { MessageContainer } from '../components/MessageContainer';

test('renders MessageContainer', () => {
  const comp = render(<MessageContainer />);
  expect(comp).toBeTruthy();
});
