import React from 'react';
import { render, fireEvent } from '@testing-library/react';
import { Layout } from '../components/Layout';

test('renders Layout', () => {
  const comp = render(<Layout handleStopMessages={() => null} isStopped={false}/>);
  expect(comp).toBeTruthy();
});

test('should stop messages', async() => {
  const handleStopMessages = jest.fn()
  const { getByText } = render(<Layout handleStopMessages={handleStopMessages} isStopped={false}/>);
  const stopButton = getByText('Stop')
  expect(stopButton).toBeInTheDocument()

  await fireEvent.click(stopButton)

  expect(handleStopMessages).toHaveBeenCalledTimes(1)
});