import React from 'react';
import { render } from '@testing-library/react';
import { CardMessage } from '../components/CardMessage';
import { Message } from '../Api';

const message: Message = {
  message: 'Lorem ipsum dolo',
  priority: 0
}

test('renders CardMessage', () => {
  const comp = render(<CardMessage msg={message}/>);
  expect(comp).toBeTruthy();
});
