export const getColorByPriority = (priority: number): string => {
  if (priority === 0) { // Error
    return '#F56236'
  } else if (priority === 1) { // Warning
    return '#FCE788'
  } else if (priority === 2) { // Info
    return '#88FCA3'
  } else {
    return 'red'
  }
}