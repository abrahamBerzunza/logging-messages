import { createContext } from 'react';
import { Message } from '../Api';

interface StoreState {
  errorMessages: Message[];
  warnMessages: Message[];
  infoMessages: Message[];
}

export const initialState: StoreState = {
  errorMessages: [],
  warnMessages: [],
  infoMessages: []
};

export const StoreContext = createContext<{
  state: StoreState;
  dispatch: React.Dispatch<any>;
}>({
  state: initialState,
  dispatch: () => null
});