import { Message } from '../Api';
import * as Types from './constants';

export const StoreReducer = (state: any, action: any) => {
  switch (action.type) {
    case Types.SET_MESSAGES_ERROR:
      return {
        ...state,
        errorMessages: [...state.errorMessages, action.payload]
      };
    case Types.SET_MESSAGES_WARN:
      return {
        ...state,
        warnMessages: [...state.warnMessages, action.payload]
      };
    case Types.SET_MESSAGES_INFO:
      return {
        ...state,
        infoMessages: [...state.infoMessages, action.payload]
      };
    case Types.CLEAR_ALL_MESSAGES:
      return {
        ...state,
        errorMessages: [],
        warnMessages: [],
        infoMessages: []
      }
    case Types.CLEAR_MESSAGE:
      const message: string = action.payload.message
      const result = state[action.payload.priority]
        .filter((msg: Message) => msg.message !== message)
      
      return {
        ...state,
        [action.payload.priority]: result
      }
    default:
      return state
  }
};