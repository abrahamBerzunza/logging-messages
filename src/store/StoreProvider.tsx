import React, { useReducer } from 'react';
import { StoreReducer } from './reducers';
import { StoreContext, initialState } from './context';

interface IStoreProviderProps {
  children?: React.ReactNode;
}

export const StoreProvider: React.FC<IStoreProviderProps> = ({ children }) => {
  const [state, dispatch] = useReducer(StoreReducer, initialState);

  return (
    <StoreContext.Provider
      value={{
        state,
        dispatch
      }}
    >
      {children}
    </StoreContext.Provider>
  );
};