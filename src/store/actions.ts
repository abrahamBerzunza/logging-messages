import { Message } from '../Api';
import * as Types from './constants';

export const setMessages = (dispatchEvent: any, message: Message) => {
  switch (+message.priority) {
    case 0: // Error
      dispatchEvent({
        type: Types.SET_MESSAGES_ERROR,
        payload: message
      });
      break;

    case 1: // Warn
      dispatchEvent({
        type: Types.SET_MESSAGES_WARN,
        payload: message
      });
      break;

    case 2: // Info
      dispatchEvent({
        type: Types.SET_MESSAGES_INFO,
        payload: message
      });
      break;
  }
}

export const clearAllMessages = (dispatchEvent: any) => {
  dispatchEvent({
    type: Types.CLEAR_ALL_MESSAGES,
  });
}

export const clearMessage = (dispatchEvent: any, message: Message) => {
  switch (+message.priority) {
    case 0: // Error
      dispatchEvent({
        type: Types.CLEAR_MESSAGE,
        payload: {
          priority: 'errorMessages',
          message: message.message
        }
      });
      break;

    case 1: // Warn
      dispatchEvent({
        type: Types.CLEAR_MESSAGE,
        payload: {
          priority: 'warnMessages',
          message: message.message
        }
      });
      break;

    case 2: // Info
      dispatchEvent({
        type: Types.CLEAR_MESSAGE,
        payload: {
          priority: 'infoMessages',
          message: message.message
        }
      });
      break;
  }
}
