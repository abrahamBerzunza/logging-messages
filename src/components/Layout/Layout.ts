import styled from 'styled-components'
import Button from '@material-ui/core/Button';

export const StyledButton = styled(Button)`
  background-color: #88FCA3;
  font-weight: bold;
  &:hover {
    background-color: #88FCA3;
  }
`