import React, { useContext } from 'react';
import Grid from '@material-ui/core/Grid';
import Typography from '@material-ui/core/Typography';
import { StyledButton } from './Layout' 
import { MessageContainer } from '../MessageContainer'
import { StoreContext } from '../../store/context';
import { clearAllMessages } from '../../store/actions';
import { CardMessage } from '../CardMessage';

interface ILayoutProps {
  handleStopMessages: () => void;
  isStopped: boolean;
}

export const Layout: React.FC<ILayoutProps> = ({ handleStopMessages, isStopped }) => {
  const { state, dispatch } = useContext(StoreContext)

  const handleClearAllMessages = () => {
    clearAllMessages(dispatch)
  }

  return (
    <Grid container>
      <Grid item xs={12} style={{ display: 'flex', justifyContent: 'center' }}>
        {
          !isStopped ? (
            <StyledButton size="small" style={{ marginRight: '8px' }} onClick={handleStopMessages}>
              Stop
            </StyledButton>
          ) : (
            <StyledButton size="small" style={{ marginRight: '8px' }} onClick={handleStopMessages}>
              Start
            </StyledButton>
          )
        }
        <StyledButton size="small" onClick={handleClearAllMessages}>
          Clear
        </StyledButton>
      </Grid>
      <Grid item xs={12} md={4}>
        <Typography variant="h6" component="h1">
          Error Type 1
        </Typography>
        <Typography variant="caption" gutterBottom>
          Count {state.errorMessages.length}
        </Typography>
        <MessageContainer>
          {
            state.errorMessages?.map(msg => (
              <CardMessage msg={msg} />
            ))
          }
        </MessageContainer>
      </Grid>
      <Grid item xs={12} md={4}>
        <Typography variant="h6" component="h1">
          Warning Type 2  
        </Typography>
        <Typography variant="caption" gutterBottom>
          Count {state.warnMessages.length}
        </Typography>
        <MessageContainer>
          {
            state.warnMessages?.map(msg => (
              <CardMessage msg={msg}  />
            ))
          }
        </MessageContainer>
      </Grid>
      <Grid item xs={12} md={4}>
        <Typography variant="h6" component="h1">
          Info Type 3
        </Typography>
        <Typography variant="caption" gutterBottom>
          Count {state.infoMessages.length}
        </Typography>
        <MessageContainer>
          {
            state.infoMessages?.map(msg => (
              <CardMessage msg={msg}  />
            ))
          }
        </MessageContainer>
      </Grid>
    </Grid>
  )
}