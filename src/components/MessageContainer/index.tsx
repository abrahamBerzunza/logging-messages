import React from 'react';
import { StyledDiv } from './MessageContainer'

interface IMessageContainerProps {
  children?: React.ReactNode
}

export const MessageContainer: React.FC<IMessageContainerProps> = ({ children }) => {
  return (
    <StyledDiv>
      { children }
      <div style={{ float:"left", clear: "both"}} />
    </StyledDiv>
  )
}