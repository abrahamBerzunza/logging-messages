import styled from 'styled-components';

export const StyledDiv = styled.div`
  padding-right: 8px;
  height: 85vh;
  overflow-y: scroll;
`