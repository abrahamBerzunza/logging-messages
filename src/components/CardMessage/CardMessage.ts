import styled from 'styled-components';
import Card from '@material-ui/core/Card';
import CardActions from '@material-ui/core/CardActions';
import { getColorByPriority } from '../../utils'

interface IStyledCardProps {
  priority: number
};

export const StyledCard = styled(Card)<IStyledCardProps>`
  background-color: ${props => getColorByPriority(props.priority)} !important;
  margin-bottom: 8px;
`;

export const StyledCardActions = styled(CardActions)`
  display: flex;
  justify-content: flex-end;
`;