import React, { useContext } from 'react';
import { Message } from '../../Api';
import { StyledCard, StyledCardActions } from './CardMessage'
import CardContent from '@material-ui/core/CardContent';
import Button from '@material-ui/core/Button';
import Typography from '@material-ui/core/Typography';
import { StoreContext } from '../../store/context';
import { clearMessage } from '../../store/actions'

interface ICardMessageProps {
  msg: Message
}

export const CardMessage: React.FC<ICardMessageProps> = ({ msg }) => {
  const { dispatch } = useContext(StoreContext);

  const handleClearMessage = () => {
    clearMessage(dispatch, msg)
  }

  return (
    <StyledCard priority={+msg?.priority}>
      <CardContent>
        <Typography color="textPrimary" gutterBottom>
          { msg?.message }
        </Typography>
      </CardContent>
      <StyledCardActions>
        <Button size="small" style={{ textTransform: 'none' }} onClick={handleClearMessage}>
          Clear
        </Button>
      </StyledCardActions>
    </StyledCard>
  )
}