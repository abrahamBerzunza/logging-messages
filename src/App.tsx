import React, { useContext, useRef, useState } from 'react';
import { useEffect } from 'react';
import generateMessage, { Message } from './Api';
import { Layout } from './components/Layout'
import { StoreContext } from './store/context';
import { setMessages } from './store/actions';

const App: React.FC = () => {
  const { dispatch } = useContext(StoreContext);
  const [isStopped, setIsStopped] = useState(false)
  const stopAction = useRef<() => void>(() => null)

  const handleStopMessages = () => {
    setIsStopped(prevState => !prevState)
    stopAction.current()
  }

  useEffect(() => {
    if (!isStopped) {
      stopAction.current = generateMessage((message: Message) => {
        setMessages(dispatch, message)
      });
  
      return stopAction.current;
    }
  // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [setMessages, isStopped]);

  return (
    <Layout isStopped={isStopped} handleStopMessages={handleStopMessages} />
  );
}

export default App;
